<?php

namespace Drupal\domain_group\QueryAccess;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\group\Plugin\GroupContentEnablerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a class for altering entity queries.
 *
 * Extends entity queries to ensure group is always joined - it isn't if a user
 * has 'by pass group' access in group module. This is then used to limit access
 * by group domain.
 *
 * @see Drupal\group\QueryAccess\EntityQueryAlter
 *
 * @internal
 */
class EntityQueryAlter implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The group content enabler plugin manager.
   *
   * @var \Drupal\group\Plugin\GroupContentEnablerManagerInterface
   */
  protected $pluginManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The query cacheable metadata.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cacheableMetadata;

  /**
   * The data table alias.
   *
   * @var string|false
   */
  protected $dataTableAlias = FALSE;

  /**
   * Constructs a new EntityQueryAlter object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $plugin_manager
   *   The group content enabler plugin manager.
   * @param \Drupal\group\Access\ChainGroupPermissionCalculatorInterface $permission_calculator
   *   The group permission calculator.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, GroupContentEnablerManagerInterface $plugin_manager, Connection $database, RendererInterface $renderer, DomainNegotiatorInterface $domain_negotiator, RequestStack $request_stack, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
    $this->database = $database;
    $this->renderer = $renderer;
    $this->domainNegotiator = $domain_negotiator;
    $this->requestStack = $request_stack;
    $this->currentUser = $current_user;
    $this->cacheableMetadata = new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.group_content_enabler'),
      $container->get('database'),
      $container->get('renderer'),
      $container->get('domain.negotiator'),
      $container->get('request_stack'),
      $container->get('current_user')
    );
  }

  /**
   * Alters the select query for the given entity type.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The select query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   */
  public function alter(SelectInterface $query, EntityTypeInterface $entity_type) {
    $this->doAlter($query, $entity_type, $query->getMetaData('op') ?: 'view');
    $this->applyCacheability();
  }

  /**
   * Actually alters the select query for the given entity type.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The select query.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param string $operation
   *   The query operation.
   */
  protected function doAlter(SelectInterface $query, EntityTypeInterface $entity_type, $operation) {
    $domain_group_config = \Drupal::config('domain_group.settings');
    if (!$domain_group_config->get('unique_group_access')) {
      // @todo everywhere? add config to $this->cacheableMetadata->addCacheTags($cache_tags);
      $this->cacheableMetadata->addCacheableDependency($domain_group_config);
      return;
    }

    $active = $this->domainNegotiator->getActiveDomain();
    if (empty($active)) {
      return;
    }
    $group_id = $active->getThirdPartySetting('domain_group', 'group');
    if (empty($group_id)) {
      return;
    }

    $entity_type_id = $entity_type->id();
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (!$storage instanceof SqlContentEntityStorage) {
      return;
    }

    $query_tables = $query->getTables();

    //
    // Group Entities.
    //
    // The group itself.
    // Are there cases where you want to be able to disable this for a query?
    if ($entity_type_id == 'group') {
      $query->condition($this->getBaseTableAlias($query_tables) . '.id', $group_id);
      return;
    }

    //
    // Group content entities.
    //
    // Entities that have a relationship with a group via a group content
    // which use plugins for content types.
    $plugin_ids = [];
    foreach ($this->pluginManager->getDefinitions() as $plugin_id => $plugin_info) {
      if ($plugin_info['entity_type_id'] == $entity_type_id) {
        $plugin_ids[] = $plugin_id;
      }
    }
    if (empty($plugin_ids)) {
      return;
    }

    /** @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface $gct_storage */
    $gct_storage = $this->entityTypeManager->getStorage('group_content_type');
    $group_content_types = $gct_storage->loadByContentPluginId($plugin_ids);

    // If any new group content entity is added using any of the retrieved
    // plugins, it might change access.
    $cache_tags = [];
    foreach ($plugin_ids as $plugin_id) {
      $cache_tags[] = "group_content_list:plugin:$plugin_id";
    }
    $this->cacheableMetadata->addCacheTags($cache_tags);

    if (empty($group_content_types)) {
      // Because we add cache tags checking for new group content above, we can
      // simply bail out here without adding any group content type related
      // cache tags because a new group content type does not change the
      // permissions until a group content is created using said group content
      // type, at which point the cache tags above kick in.
      return;
    }

    if (isset($query_tables['gcfd']) && $query_tables['gcfd']['table'] == 'group_content_field_data') {
      // Group has left joined the table already.
      $query->condition('gcfd.gid', $group_id);
    }
    else {
      // Group permissions and plugin access rules mean the table isn't joined
      // yet.
      // The base table is usually aliased, so let's try and find it.
      $base_table = $this->getBaseTableAlias($query_tables) ?: $entity_type->getBaseTable();
      $id_key = $entity_type->getKey('id');
      $query->innerJoin(
        'group_content_field_data',
        'gcfd_domain',
        "$base_table.$id_key=gcfd_domain.entity_id AND gcfd_domain.type IN (:group_content_type_ids_in_use[])",
#        [':group_content_type_ids_in_use[]' => $group_content_type_ids_in_use]
        [':group_content_type_ids_in_use[]' => array_keys($group_content_types)]
      );
      $query->condition('gcfd_domain.gid', $group_id);
    }
  }

  /**
   * Find the alias of the base table.
   */
  private function getBaseTableAlias($query_tables) {
    foreach ($query_tables as $alias => $table) {
      if ($table['join type'] === NULL) {
        return $alias;
      }
    }
  }

  /**
   * Applies the cacheablity metadata to the current request.
   */
  protected function applyCacheability() {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->isMethodCacheable() && $this->renderer->hasRenderContext() && $this->hasCacheableMetadata()) {
      $build = [];
      $this->cacheableMetadata->applyTo($build);
      $this->renderer->render($build);
    }
  }

  /**
   * Check if the cacheable metadata is not empty.
   *
   * An empty cacheable metadata object has no context, tags, and is permanent.
   *
   * @return bool
   *   TRUE if there is cacheability metadata, otherwise FALSE.
   */
  protected function hasCacheableMetadata() {
    return $this->cacheableMetadata->getCacheMaxAge() !== Cache::PERMANENT
      || count($this->cacheableMetadata->getCacheContexts()) > 0
      || count($this->cacheableMetadata->getCacheTags()) > 0;
  }

}
