## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

This module provides integration between Domain and Group modules.

You will find a new Domain Settings tab in the group to set a
domain and other useful configurations.

## REQUIREMENTS

  * Domain - https://www.drupal.org/project/domain.
  * Domain site settings - https://www.drupal.org/project/domain_site_settings.
  * Group - https://www.drupal.org/project/group.


## INSTALLATION

  * Install normally as other modules are installed. For support:
    https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules


## CONFIGURATION

  * The configuration options for a group are available via:
    _/group/[GROUP ID]/domain-settings_
  * General settings are available via:
    _/admin/config/domain/domain-group_


## MAINTAINERS

Current maintainers:
 * Ivan Duarte (jidrone) - https://www.drupal.org/u/jidrone
 * Fabian Sierra (fabiansierra5191) - https://www.drupal.org/u/fabiansierra5191
