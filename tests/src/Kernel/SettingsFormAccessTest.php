<?php

namespace Drupal\Tests\domain_group\Kernel;

use Drupal\domain_group\ContextProvider\DomainGroupContext;
use Drupal\domain_group\DomainGroupResolverInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\domain_group\Traits\GroupCreationTrait;
use Drupal\Tests\domain_group\Traits\InitializeGroupsTrait;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;
use Drupal\domain_group\Form\DomainGroupSettingsForm;

/**
 * \Drupal\domain_group\Form\DomainGroupSettingsForm::access
 *
 * @group domain_group
 */
class SettingsFormAccessTest extends GroupKernelTestBase {

  /**
   * The group type we will use to test access on.
   *
   * @var \Drupal\group\Entity\GroupType
   */
  protected $groupType;

  /**
   * The group we will use to test access on.
   *
   * @var \Drupal\group\Entity\Group
   */
  protected $group;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'domain',
    'domain_group',
    'path_alias',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['user']);
    $this->groupType = $this->createGroupType(['id' => 'foo', 'creator_membership' => FALSE]);
    $this->group = $this->createGroup(['type' => 'foo']);
  }

  public function testFormAccess() {
    $form = new DomainGroupSettingsForm($this->container->get('plugin.manager.domain_group_settings'));

    // Non-member.
    $this->assertFalse($form->access($this->group, $this->getCurrentUser())->isAllowed());

    // Member. 
    $this->group->addMember($this->getCurrentUser());
    $this->assertFalse($form->access($this->group, $this->getCurrentUser())->isAllowed());

    // Permission to one plugin.
    $this->groupType->getMemberRole()->grantPermission('administer group domain settings')->save();
    $this->assertTrue($form->access($this->group, $this->getCurrentUser())->isAllowed());

    // Admin
    $admin = $this->createUser([], ['bypass domain group permissions']);
    $this->assertTrue($form->access($this->group, $admin)->isAllowed());
  }

}
