<?php

namespace Drupal\Tests\domain_group\Functional;

use Drupal\Tests\domain_group\Traits\GroupCreationTrait;
use Drupal\Tests\domain_group\Traits\InitializeGroupsTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the group and group content access.
 *
 * @group domain_group
 */
class UniqueGroupAccessTest extends BrowserTestBase {

  use GroupCreationTrait;
  use InitializeGroupsTrait;

  /**
   * Will be removed when issue #3204455 on Domain Site Settings gets merged.
   *
   * See https://www.drupal.org/project/domain_site_settings/issues/3204455.
   *
   * @var bool
   *
   * @see \Drupal\Core\Config\Testing\ConfigSchemaChecker
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'block',
    'group',
    'gnode',
    'domain',
    'domain_site_settings',
    'domain_group',
    'views'
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * Regular authenticated User for tests.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * User administrator of group 1.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $groupAdmin;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create test user.
    $this->testUser = $this->drupalCreateUser([
      'access content',
      'access group overview',
    ]);

    // Create group admin.
    $this->groupAdmin = $this->drupalCreateUser([
      'access content',
      'access group overview',
    ]);

    // Setup the group types and test groups from the InitializeGroupsTrait.
    $this->initializeTestGroups(['uid' => $this->groupAdmin->id()]);
    $this->initializeTestGroupsDomains();
    $this->initializeTestGroupContent();

    // Allow anonymous to view groups of type A.
    $this->groupTypeA->getAnonymousRole()->grantPermissions([
      'view group',
    ])->save();

    // Allow outsider to view group content article of type A.
    $this->groupTypeA->getOutsiderRole()->grantPermissions([
      'view group',
      'view group_node:article entity',
    ])->save();

    // Allow member to view, edit, delete group content article of type A.
    $this->groupTypeA->getMemberRole()->grantPermissions([
      'view group',
      'access content overview',
      'administer group',
      'administer members',
    ])->save();

  }

  /**
   * Test group access when unique group access is enabled.
   */
  public function testUniqueGroupAccess() {
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());

    $this->drupalLogin($this->testUser);
    // Listing
    $this->drupalGet('admin/group');
    $this->assertSession()->pageTextContains($this->groupA1->label());
    $this->assertSession()->pageTextContains($this->groupA2->label());
    // Listing
    $this->drupalGet($ga1_domain->getPath() . '/admin/group');
    $this->assertSession()->pageTextContains($this->groupA1->label());
    $this->assertSession()->pageTextNotContains($this->groupA2->label());
    // Listing
    $this->drupalGet($ga2_domain->getPath() . '/admin/group');
    $this->assertSession()->pageTextNotContains($this->groupA1->label());
    $this->assertSession()->pageTextContains($this->groupA2->label());

    $this->drupalLogout();
    // Visiting groups from default domain.
    $this->drupalGet('group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(200);

    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    // Visiting group A1 page.
    $this->drupalGet($ga1_domain->getPath());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA3->id());
    $this->assertSession()->statusCodeEquals(403);

    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());
    // Visiting group A2 page.
    $this->drupalGet($ga2_domain->getPath());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups should be denied.
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA3->id());
    $this->assertSession()->statusCodeEquals(403);
    
  }

  /**
   * Test group access when unique group access is disabled.
   */
  public function testUniqueGroupAccessDisabled() {
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());

    // Disable unique group access.
    $this->setUniqueGroupAccess(FALSE);

    // Visiting groups from default domain.
    $this->drupalGet('group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(200);

    // Visiting other groups from group domain should be allowed.
    $this->drupalGet($ga1_domain->getPath());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/group/' . $this->groupA3->id());
    $this->assertSession()->statusCodeEquals(200);

    // Visiting other groups from group domain should be allowed.
    $this->drupalGet($ga2_domain->getPath());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga2_domain->getPath() . '/group/' . $this->groupA3->id());
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalLogin($this->testUser);
    // Listing
    $this->drupalGet('admin/group');
    $this->assertSession()->pageTextContains($this->groupA1->label());
    $this->assertSession()->pageTextContains($this->groupA2->label());
    // Listing
    $this->drupalGet($ga1_domain->getPath() . '/admin/group');
    $this->assertSession()->pageTextContains($this->groupA1->label());
    $this->assertSession()->pageTextContains($this->groupA2->label());
    // Listing
    $this->drupalGet($ga2_domain->getPath() . '/admin/group');
    $this->assertSession()->pageTextContains($this->groupA1->label());
    $this->assertSession()->pageTextContains($this->groupA2->label());
  }

  /**
   * Test content access when unique group access is enabled.
   */
  public function testUniqueContentNodeAccess() {
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());

    // First as privileged user with access, then with less access to check
    // caching at the same time.
    $this->drupalLogin($this->groupAdmin);
    // Visiting groups from default domain.
    $this->drupalGet('node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Can edit own.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Can delete own.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet('node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextContains($this->nodeA2->label());

    // Visiting group A1 content.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(403);
    // Editing own on correct domain.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting own on correct domain.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet($ga1_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());

    // Visiting group A2 content.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet($ga2_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());
    // Editing own on other domain denied.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA3->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting own on other domain domain.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA3->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);

    // Same checks as user with only view permissions on the group.
    $this->drupalLogin($this->testUser);
    // Visiting groups from default domain.
    $this->drupalGet('node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Editing should be denied.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting should be denied.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet('node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextContains($this->nodeA2->label());

    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    // Visiting group A1 content.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(403);
    // Editing should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet($ga1_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());

    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());
    // Visiting group A2 content.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet($ga2_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());
  }

  /**
   * Test content access when unique group access is disabled.
   */
  public function testUniqueContentNodeAccessDisabled() {
    $this->drupalLogin($this->testUser);
    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    // Disable unique group access.
    $this->setUniqueGroupAccess(FALSE);

    // Visiting groups from default domain.
    $this->drupalGet('node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Editing should be denied.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting should be denied.
    $this->drupalGet('node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet('node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextContains($this->nodeA2->label());

    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    // Visiting group A1 content.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(200);
    // Editing should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/edit');
    $this->assertSession()->statusCodeEquals(403);
    // Deleting should be denied.
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA1->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA2->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . '/node/' . $this->nodeA3->id() . '/delete');
    $this->assertSession()->statusCodeEquals(403);
    // Content list.
    $this->drupalGet($ga1_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextContains($this->nodeA2->label());
    $this->assertSession()->pageTextContains($this->nodeA3->label());

    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());
    // Visiting group A2 content.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA2->id());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA1->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($ga2_domain->getPath() . '/node/' . $this->nodeA3->id());
    $this->assertSession()->statusCodeEquals(200);
    // Content list.
    $this->drupalGet($ga2_domain->getPath() . '/node');
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextContains($this->nodeA2->label());
    $this->assertSession()->pageTextContains($this->nodeA3->label());
  }

  /**
   * Test content access when unique group access is enabled.
   */
  public function testUniqueGroupContentAccess() {
    // Allow anonymous to view groups content relation of type A.
    $this->groupTypeA->getAnonymousRole()->grantPermissions([
      'view group_node:article content',
    ])->save();

    $domain_storage = \Drupal::entityTypeManager()->getStorage('domain');
    $ga1_domain = $domain_storage->load('group_' . $this->groupA1->id());
    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());

    $a1_content = $this->groupA1->getContent('group_node:article');
    $this->assertCount(1, $a1_content);
    $a1_content = reset($a1_content);
    $a2_content = $this->groupA2->getContent('group_node:article');
    $this->assertCount(1, $a2_content);
    $a2_content = reset($a2_content);
    $a3_content = $this->groupA3->getContent('group_node:article');
    $this->assertCount(1, $a3_content);
    $a3_content = reset($a3_content);

    //
    // First as admin.
    //
    $this->drupalLogin($this->groupAdmin);
    // Default domain content accessible.
    $this->drupalGet($a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    // But outsider doesn't have access to content on other group.
    $this->drupalGet($a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    // And edit own.
    $this->drupalGet($a1_content->toUrl('edit-form')->toString());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($a1_content->toUrl('delete-form')->toString());
    $this->assertSession()->statusCodeEquals(200);
    // Other not.
    $this->drupalGet($a2_content->toUrl('edit-form')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($a2_content->toUrl('delete-form')->toString());
    $this->assertSession()->statusCodeEquals(403);

    // Visiting group A1 content.
    $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . $a3_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    // Edit delete own on domain.
    foreach (['edit-form', 'delete-form'] as $form) { 
      $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(200);
      $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga1_domain->getPath() . $a3_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
    }

    // Can't access group 1 content on domain 2.
    // Can't view group 2 content as outsider.
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a3_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    // Edit delete own not as not on domain.
    foreach (['edit-form', 'delete-form'] as $form) { 
      $this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga2_domain->getPath() . $a3_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
    }

    // Content list.
    $this->drupalGet($a1_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());
    $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->nodeA1->label());
    $this->assertSession()->pageTextNotContains($this->nodeA2->label());
    $this->assertSession()->pageTextNotContains($this->nodeA3->label());
    // @todo Can still access route collection on other domains, if has access
    //   on default domain.
    //
    // Could decorate GroupPermissionChecker.
    // 
    //$this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl('collection')->toString());
    //$this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);

    //
    // Then as anon with access to group content entities.
    //
    $this->drupalLogout();
    // Default domain content accessible.
    $this->drupalGet($a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    // But edit and delete not.
    $this->drupalGet($a1_content->toUrl('edit-form')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($a1_content->toUrl('delete-form')->toString());
    $this->assertSession()->statusCodeEquals(403);

    // Visiting group A1 content.
    $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . $a3_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    // Edit delete not.
    foreach (['edit-form', 'delete-form'] as $form) { 
      $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga1_domain->getPath() . $a3_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
    }

    $ga2_domain = $domain_storage->load('group_' . $this->groupA2->id());
    // Visiting group A1 content.
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(200);
    // Visiting other groups' content should be denied.
    $this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a3_content->toUrl()->toString());
    $this->assertSession()->statusCodeEquals(403);
    // Edit delete not.
    foreach (['edit-form', 'delete-form'] as $form) { 
      $this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
      $this->drupalGet($ga2_domain->getPath() . $a3_content->toUrl($form)->toString());
      $this->assertSession()->statusCodeEquals(403);
    }
    // Content list.
    $this->drupalGet($a1_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . $a1_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a1_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga1_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet($ga2_domain->getPath() . $a2_content->toUrl('collection')->toString());
    $this->assertSession()->statusCodeEquals(403);
  }


}
